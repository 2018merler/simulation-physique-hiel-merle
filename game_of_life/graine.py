import numpy as np
import random as rd
from generate_universe import *

def implantation_graine(graine, size_universe) :
    """implante la graine dans l'univers, avec graine un tableau numpy en 2 dimensions, et size_universe un tuple"""
    universe = generate_universe(size_universe)

    ligne_graine, colonne_graine = np.shape(graine)
    ligne_universe, colonne_universe = size_universe[0], size_universe[1]

    if ligne_graine > ligne_universe or colonne_graine > colonne_universe :
        return "Dimensions incompatibles."

    else :
        ligne, colonne = rd.randint(0, ligne_universe - 1), rd.randint(0, colonne_universe - 1) #on choisit une case aléatoire dans notre univers, qui correspondra au bord supérieur gauce de notre graine

        for i in range (ligne_graine) :
            for j in range (colonne_graine) :
                universe[(ligne + i) % ligne_universe][(colonne + j) % colonne_universe] = graine[i][j]  #on utilise les congruences pour respecter le fait que le tableau n'a pas de bord

        return universe


