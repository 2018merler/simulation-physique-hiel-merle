from graine import *
from generate_universe import *
import matplotlib.pyplot as plt
import numpy as np

def Affichage_universe(universe):
    size_universe = np.shape(universe)
    colonnes_universe, lignes_universe = size_universe[1], size_universe[1]
    A = np.zeros((lignes_universe,colonnes_universe,3), dtype = 'uint8')
    for i in range(lignes_universe):
        for j in range(colonnes_universe):
            if universe[i][j] != 0:
                A[i][j] = (0, 0, 0)
            else:
                A[i][j] = (255, 255, 255)
    plt.imshow(A)
    plt.show()

Affichage_universe(implantation_graine([(0,1),(1,1)], (8,8)))
