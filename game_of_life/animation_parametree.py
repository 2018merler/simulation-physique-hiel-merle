import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as anim
from Etape_jeu_de_la_vie import *
from Seeds import *
from generate_universe import *

def implantation_graine(graine, size_universe, x, y) :
    """implante la graine avec le bord gauche supérieur aux coordonnées données en argument dans l'univers, avec graine un tableau numpy en 2 dimensions, et size_universe un tuple"""
    universe = generate_universe(size_universe)

    ligne_graine, colonne_graine = np.shape(graine)
    ligne_universe, colonne_universe = size_universe[0], size_universe[1]

    if ligne_graine > ligne_universe or colonne_graine > colonne_universe :
        return "Dimensions incompatibles."

    else :
        for i in range (ligne_graine) :
            for j in range (colonne_graine) :
                universe[(x + i) % ligne_universe][(y + j) % colonne_universe] = graine[i][j]  #on utilise les congruences pour respecter le fait que le tableau n'a pas de bord

        return universe



def animation_paramétrée(universe_size, seed, seed_position, cmap, n_generations, interval, save):
    universe = implantation_graine(seed, universe_size, seed_position[0], seed_position[1])
    fig = plt.figure()
    im = plt.imshow(universe, cmap=cmap, animated = True)
    def update(i):
        universe = im.get_array()
        im.set_array(etape_jeu_de_la_vie(universe))
        return im,
    ani = anim.FuncAnimation(fig, update, frames=n_generations, interval = interval, blit=save)
    plt.show()



animation_paramétrée((6,6),[(0,1,0),(0,1,0),(0,1,0)], (1,1), 'Blues', 30, 300, True)
