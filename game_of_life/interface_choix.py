def interface_choix() :

    var_texte = StringVar()
    ligne_texte = Entry(window, textvariable=var_texte, width=30)
    ligne_texte.pack()

    var_choix = StringVar()
    taille_1 = Radiobutton(window, text="6x6", variable=var_choix, value= 6)
    taille_2 = Radiobutton(window, text="8x8", variable=var_choix, value=8)
    taille_3 = Radiobutton(window, text="10x10", variable=var_choix, value=10)
    taille_1.pack()
    taille_2.pack()
    taille_3.pack()
