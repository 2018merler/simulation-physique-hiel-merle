from tkinter import *
import numpy as np
import time
from Etape_jeu_de_la_vie import *

def afficher_univers_propre(univers) :

    size = np.shape(univers)

    t=list(range(size[0]))
    r=list(   range(size[0]))

    f=Tk() #création de la fenêtre

    for i in range(size[0]):
        t[i]=list(range(size[1]))
        r[i]=list(range(size[1]))
        for j in range(size[1]): # création de 2 tableaux (3;4) et d'une grille (3;4)
            t[i][j]=Canvas(f,height=50,width=50) # création d'un canevas mis dans le tableau t aux indices i,j
            t[i][j].grid(row=i,column=j) # création de la case i,j de la grille dans laquelle on y met le canevas précédent
            t[i][j].create_rectangle(5,5,50,50) #dessine un rectangle d'à peu près la dimension du canevas dans ce canevas
            if univers[i][j] == 1 :
                r[i][j]=t[i][j].create_oval(15,15,40,40,outline='black',fill='black') # création d'un rond dans le canevas précédent mis dans le tableau
            # r aux indices i,j

    f.mainloop()



#utiliser update pour n'avoir qu'une fenêtre
#utiliser time pour gérer le temps entre deux updates


def film(univers,n) :
    afficher_univers_propre(univers)
    for _ in range (n) :
        univers = etape_jeu_de_la_vie(univers)
        afficher_univers_propre(univers)


film(np.array([[0,0,0,0,0,0],[0,1,1,0,0,0], [0,1,1,0,0,0], [0,0,0,1,1,0], [0,0,0,1,1,0], [0,0,0,0,0,0]]), 15)

