import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as anim
from Etape_jeu_de_la_vie import *

def animation() :
    univers = np.array([[0,0,0,0,0,0], [0,1,1,0,0,0], [0,1,1,0,0,0], [0,0,0,1,1,0], [0,0,0,1,1,0], [0,0,0,0,0,0]])
    plt.imshow(univers)
    fig = plt.figure()  #On initialise la figure que l'on va afficher
    im = plt.imshow(univers, cmap = 'Greys', animated = True) #On transforme univers en image

    def animate(i):
        """Création d'une fonction que l'on va itérer dans FuncAnimation"""
        univers = im.get_array()  #on récupère univers sous forme de tableau
        im.set_array(etape_jeu_de_la_vie(univers))  #On actualise univers (on lui fait frnchir une génération)

        return im,

    anime = anim.FuncAnimation(fig, animate, frames = 10, interval = 500, blit = True)
    plt.show()

animation()
