import numpy as np

def traitement_cellule(univers, x, y) :
    liste_voisins = [[x-1, y], [x-1, y+1], [x-1, y-1], [x, y+1], [x, y-1], [x+1,y], [x+1, y+1], [x+1, y-1]] #On suppose ici que le tableau possède plus de 3 lignes et de 3 colonnes
    nb_cellules_vivantes = 0
    ligne_univers, colonne_univers = np.shape(univers)
    for voisin in liste_voisins :
        if univers[voisin[0]%ligne_univers][voisin[1]%colonne_univers] != 0 : #on utilise les congruences pour respecter le fait que l'univers n'est pas fermé
            nb_cellules_vivantes += 1

    if nb_cellules_vivantes == 3 :
        return True
    elif nb_cellules_vivantes != 2 :
        return False
    else :
        return univers[x][y] == 1


