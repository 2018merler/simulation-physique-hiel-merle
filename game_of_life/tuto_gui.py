""""from tkinter import *

# Window creation, root of the interface
window = Tk()

# Creation of a label (text line) that says Hello World ! and with as first parameter the previous window
label_field = Label(window,text="Hello World !")

# Display of the label
label_field.pack()

#On ajoute un bouton permettant de quitter la fenêtre
bouton_quitter = Button(window, text="Quitter", command=window.quit)
bouton_quitter.pack()

#On ajoute un champ permettant à l'utilisateur d'écrire
var_texte = StringVar()
ligne_texte = Entry(window, textvariable=var_texte, width=30)
ligne_texte.pack()

#On ajoute une case que l'on peut cocher ou décocher
var_case = IntVar()
case = Checkbutton(window, text="Ne plus poser cette question", variable=var_case)
case.pack()

#Donne accès à l'état de la case (cochée ou décochée)
var_case.get()

#On ajoute des boutons que l'on peut cocher, sachant que si on en coche un, les autres se décochent automatiquement
var_choix = StringVar()
choix_rouge = Radiobutton(window, text="Rouge", variable=var_choix, value="rouge")
choix_vert = Radiobutton(window, text="Vert", variable=var_choix, value="vert")
choix_bleu = Radiobutton(window, text="Bleu", variable=var_choix, value="bleu")
choix_rouge.pack()
choix_vert.pack()
choix_bleu.pack()

#Donne accès à l'état du bouton sélectionné
var_choix.get()

#On ajoute une liste déroulante avec des choix préétablis par le développeur
liste = Listbox(window)
liste.pack()
liste.insert(END, "Pierre")
liste.insert(END, "Feuille")
liste.insert(END, "Ciseau")


cadre = Frame(window, width=768, height=576, borderwidth=1)
cadre.pack(fill=BOTH)
message = Label(cadre, text="Notre fenêtre")
message.pack(side="top", fill=X)



# Running of the Tkinter loop that ends when we close the windw
window.mainloop()"""




"""def write_text(): #Ce code ouvre une fenêtre sur laquelle on a le choix entre appuyer sur un bouton QUIT (et alors, la fenêtre se ferme) ou sur un bouton Hello (et il ne se passe rien)
    print("Hello CentraleSupelec") #Les boutons sont centrés dans la fenêtre

root = tk.Tk()
frame = tk.Frame(root)
frame.pack()

button = tk.Button(frame,
                   text="QUIT",
                   activebackground = "blue",
                   fg="red",
                   command=quit)
button.pack(side=tk.LEFT)
slogan = tk.Button(frame,
                   fg="blue",
                   text="Hello",
                   command=write_text)
slogan.pack(side=tk.LEFT)

root.mainloop()"""

"""from tkinter import Tk, StringVar, Label, Entry, Button
from functools import partial

def update_label(label, stringvar): #cette fonction ouvre une fenêtre sur laquelle il y a une inscription "votre nom", suivie d'une case où l'on peut entrer son nom, puis d'iun bouton 'clic'
                                    #Lorsque l'on appuie sur clic, le texte entré dans le cadre remplace l'inscription initiale
                                    #Tous ces éléments se trouvent en haut à gauche de la fenêtre
    "
    Met à jour le texte d'un label en utilisant une StringVar.
    "
    text = stringvar.get()
    label.config(text=text)
    stringvar.set('merci')

root = Tk()
text = StringVar(root)
label = Label(root, text='Your name')
entry_name = Entry(root, textvariable=text)
button = Button(root, text='clic',
                command=partial(update_label, label, text))

label.grid(column=0, row=0)
entry_name.grid(column=0, row=1)
button.grid(column=0, row=2)

root.mainloop()"""

"""from tkinter import Tk, Label, Frame #On ouvre une fenêtre dans laquelle on trouve les textes suivants, en haut à gauche, avec la première inscription encadrée

root = Tk()
f1 = Frame(root, bd=1, relief='solid')
Label(f1, text='je suis dans F1').grid(row=0, column=0)
Label(f1, text='moi aussi dans F1').grid(row=0, column=1)

f1.grid(row=0, column=0)
Label(root, text='je suis dans root').grid(row=1, column=0)
Label(root, text='moi aussi dans root').grid(row=2, column=0)

root.mainloop()"""

"""from tkinter import *  #Ouvre une fenêtre avec un champ d'écriture. Dès que l'on écrit une lettre, une inscription 'Hello' apparaît
from pprint import pformat

def print_bonjour(i):
    label.config(text="Hello")

root = Tk()
frame = Frame(root, bg='white', height=100, width=400)
entry = Entry(root)
label = Label(root)

frame.grid(row=0, column=0)
entry.grid(row=1, column=0, sticky='ew')
label.grid(row=2, column=0)

frame.bind('<ButtonPress>', print_bonjour)
entry.bind('<KeyPress>', print_bonjour)
root.mainloop()"""












