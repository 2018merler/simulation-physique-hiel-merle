import numpy as np
from traitement_cellule import *


def etape_jeu_de_la_vie(univers) :
    """Il suffit de traiter chaque cellule de l'univers"""
    ligne_univers, colonne_univers = np.shape(univers)
    univers_bis = np.copy(univers)
    for ligne in range (ligne_univers) :
        for colonne in range (colonne_univers) :
            resultat = traitement_cellule(univers, ligne, colonne)
            if resultat :
                univers_bis[ligne][colonne] = 1
            else :
                univers_bis[ligne][colonne] = 0
    return univers_bis

etape_jeu_de_la_vie(np.array([[0,0,0,0,0,0], [0,1,1,0,0,0], [0,1,1,0,0,0], [0,0,0,1,1,0], [0,0,0,1,1,0], [0,0,0,0,0,0]]))


